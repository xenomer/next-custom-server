# next-custom-server-manager

next-custom-server-manager is a npm package that allows you to override next.js's default
backend and run custom logic (like scheduled work, filesystem reads/writes and
other proper backend stuff).

It comes with a remote function calls for ease of usage.

Note: I made this mainly for my own usage so continue with that expectation :)

# Installation

```bash
npm install next-custom-server-manager

# or

yarn add next-custom-server-manager
```

# Usage

TODO

# Support

See [Contributing](#contributing), I most likely am not going to fix issues myself.

# Contributing

Make a PR :).
import _ from 'lodash';
import type { NextApiRequest, NextApiResponse } from 'next';

type EndpointHandlerType<T = any> = (req: NextApiRequest, res: NextApiResponse<T>) => any;
type EndpointHandlerConfig = {};
type EndpointValueHandlerConfig = EndpointHandlerConfig & {
  /**
   * null to return falsey value with 200
   */
  falseyStatus?: number | null;
  errorStatus?: number;
};

export class NextEndpointCreator {
  private mixins: EndpointHandlerType<any>[] = [];
  handlers: Record<
    string,
    {
      handler: EndpointHandlerType;
      config: EndpointHandlerConfig;
    }
  > = {};
  globalConfig: EndpointHandlerConfig = {};
  static create() {
    return new NextEndpointCreator();
  }
  private getConfig(config: EndpointHandlerConfig) {
    return _.defaults(config, this.globalConfig);
  }
  private addHandler(method: string, handler: EndpointHandlerType, config: EndpointHandlerConfig) {
    this.handlers[method] = {
      handler,
      config: this.getConfig(config),
    };
    return this;
  }

  private async handle(req: NextApiRequest, res: NextApiResponse) {
    const { handler, config } = this.handlers[req.method || 'GET'] ?? {};

    if (handler) {
      for (const mixin of this.mixins) {
        await mixin(req, res);

        if (res.headersSent) {
          return;
        }
      }
      return handler(req, res);
    } else {
      return res.status(405).end();
    }
  }

  mixin(cb: EndpointHandlerType<any>) {
    this.mixins.push(cb);
    return this;
  }

  get<T>(callback: EndpointHandlerType<T>, config: EndpointHandlerConfig = {}) {
    return this.addHandler('GET', callback, config);
  }
  head(callback: EndpointHandlerType<undefined>, config: EndpointHandlerConfig = {}) {
    return this.addHandler('HEAD', callback, config);
  }
  post<T>(callback: EndpointHandlerType<T>, config: EndpointHandlerConfig = {}) {
    return this.addHandler('POST', callback, config);
  }
  put<T>(callback: EndpointHandlerType<T>, config: EndpointHandlerConfig = {}) {
    return this.addHandler('PUT', callback, config);
  }
  delete<T>(callback: EndpointHandlerType<T>, config: EndpointHandlerConfig = {}) {
    return this.addHandler('DELETE', callback, config);
  }
  patch<T>(callback: EndpointHandlerType<T>, config: EndpointHandlerConfig = {}) {
    return this.addHandler('PATCH', callback, config);
  }

  getMock<T>(callback: EndpointHandlerType<T> | T, config: EndpointHandlerConfig = {}) {
    return this.addHandler(
      'GET',
      _.isFunction(callback)
        ? callback
        : (req, res) => {
            return res.status(200).json(callback);
          },
      config,
    );
  }

  getValue<T>(callback: (req: NextApiRequest) => T | Promise<T>, config: EndpointValueHandlerConfig = {}) {
    const { falseyStatus = 200, errorStatus = 500 } = config;
    return this.addHandler(
      'GET',
      async (req, res) => {
        try {
          const value = await callback(req);
          if (value || falseyStatus == null) {
            res.status(200).json(value);
          } else {
            res.status(falseyStatus).end();
          }
        } catch (e: any) {
          res.status(errorStatus || 500).json({ error: e.message });
        }
      },
      config,
    );
  }

  end() {
    return this.handle.bind(this);
  }
}

import { EventEmitter } from 'events';

jest.mock('next', () => {
  return () => ({
    getRequestHandler: () => () => void 0,
    prepare: () => void 0,
    close: () => void 0,
  });
});
jest.mock('http', () => {
  return {
    createServer: (reqCb: any) =>
      new (class extends EventEmitter {
        listen(port: number, cb: Function) {
          cb();
        }
        close() {
          this.emit('close');
        }
        _send(request: Partial<import('http').IncomingMessage>) {
          reqCb(request);
        }
      })(),
  };
});

import _ from 'lodash';
import { NextCustomServer, NextCustomServerTaskConfig, NextCustomServerTask } from '../index';
const runTask = (fn: () => any, options?: Partial<NextCustomServerTaskConfig>) => ({
  name: 'task',
  run: fn,
  ...options,
});
const waitFor = (proc: import('child_process').ChildProcess): Promise<void> => {
  return new Promise((res: Function) => {
    if (proc.exitCode !== null) {
      res();
    } else {
      proc.on('close', res as any);
    }
  });
};

const tasksProcess: NextCustomServerTaskConfig[] = [
  { name: 'sync', command: 'echo', stdio: 'ignore', executeSync: true, env: ['development'] },
  { name: 'async-error-dev', command: 'return 1', stdio: 'ignore', env: ['development'] },
  { name: 'async', command: 'sleep 0.001', stdio: 'ignore' },
  { name: 'async-error-prod', command: 'return 1', stdio: 'ignore', env: ['production'] },
  { name: 'async-exit-prod', command: 'sleep 0.001', stdio: 'ignore', env: ['production'] },
];

test('dev server with run tasks', async () => {
  const mockFn = jest.fn();
  const server = new NextCustomServer({
    dev: true,
    tasks: [runTask(mockFn), runTask(mockFn, { env: ['development'] }), runTask(mockFn, { env: ['production'] })],
  });

  await server.start();

  expect(mockFn).toBeCalledTimes(2);
  expect(server.tasks).toHaveLength(2);

  await server.stop();
});

test('prod server with run tasks', async () => {
  const mockFn = jest.fn();
  const server = new NextCustomServer({
    dev: false,
    tasks: [runTask(mockFn), runTask(mockFn, { env: ['development'] }), runTask(mockFn, { env: ['production'] })],
  });

  await server.start();

  expect(mockFn).toBeCalledTimes(2);
  expect(server.tasks).toHaveLength(2);

  await server.stop();
});

test('dev server with process tasks', async () => {
  const consoleLogSpy = jest.spyOn(console, 'log').mockImplementation();
  const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();
  consoleLogSpy.mockClear();
  consoleErrorSpy.mockClear();

  const server = new NextCustomServer({
    dev: true,
    tasks: tasksProcess,
  });

  await server.start();

  expect(server.tasks).toHaveLength(2);

  const [_asyncError, _async] = server.tasks as NextCustomServerTask[];
  _asyncError.process?.kill('SIGKILL');
  await Promise.all(server.tasks!.map((task) => waitFor(task.process as any)));

  expect(consoleLogSpy).toBeCalledTimes(1);
  expect(consoleErrorSpy).toBeCalledTimes(1);
  expect(consoleErrorSpy).toBeCalledWith('task %s has exited with error %o', 'async-error-dev', expect.anything());

  await server.stop();
});

test('prod server with process tasks', async () => {
  const consoleLogSpy = jest.spyOn(console, 'log'); /* .mockImplementation() */
  const consoleErrorSpy = jest.spyOn(console, 'error'); /* .mockImplementation() */
  consoleLogSpy.mockClear();
  consoleErrorSpy.mockClear();

  const server = new NextCustomServer({
    dev: false,
    tasks: tasksProcess,
  });

  await server.start();

  expect(server.tasks).toHaveLength(3);

  const [_async, _asyncError, _asyncExit] = server.tasks as NextCustomServerTask[];

  _asyncError.process?.kill('SIGKILL');
  await Promise.all(server.tasks!.map((task) => waitFor(task.process as any)));

  await new Promise((r) => setTimeout(r, 500));

  expect(consoleLogSpy).toBeCalledTimes(2);
  expect(consoleLogSpy).toBeCalledWith('task %s has exited with code %d', 'async', 0);
  expect(consoleLogSpy).toBeCalledWith('task %s has exited with code %d', 'async-exit-prod', 0);
  expect(consoleErrorSpy).toBeCalledTimes(1);
  expect(consoleErrorSpy).toBeCalledWith('task %s has exited with error %o', 'async-error-prod', expect.anything());

  await server.stop();
});

test('no run or command', async () => {
  const server = new NextCustomServer({
    dev: true,
    tasks: [{ name: 'task' }],
  });
  await expect(server.start()).rejects.toThrow();
  expect(server.tasks).toBeUndefined();
  await server.stop();
});

test('simple request', async () => {
  const server = new NextCustomServer({
    dev: false,
    tasks: [],
  });
  await server.start();
  (server.server as any)._send({
    url: '/test',
  });
  await server.stop();
});

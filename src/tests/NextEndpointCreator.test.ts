import { NextApiRequest, NextApiResponse } from 'next';
import { NextEndpointCreator } from '../NextEndpointCreator';
class FakeResponse {
  public statusCode: number = 0;
  public data: any;
  public headersSent: boolean = false;
  constructor() {}
  public status(status: number) {
    this.statusCode = status;
    this.headersSent = true;
    return this;
  }
  public send(data: any) {
    this.data = data;
    return this;
  }
  public json(data: any) {
    this.data = data;
    return this;
  }
  public end() {
    return this;
  }
}

const testValueResponse = 'test value!';
let response: FakeResponse;
let creator: NextEndpointCreator;
beforeEach(() => {
  creator = NextEndpointCreator.create();
  response = new FakeResponse();
});

describe('methods', () => {
  test('get', async () => {
    creator.get<string>((req, res) => {
      res.status(200).send(testValueResponse);
    });
    await creator.end()(
      {
        method: 'GET',
        url: '/',
      } as NextApiRequest,
      response as any as NextApiResponse,
    );
    expect(response.statusCode).toBe(200);
    expect(response.data).toBe(testValueResponse);
  });
  test('head', async () => {
    creator.head((req, res) => {
      res.status(200).end();
    });
    await creator.end()(
      {
        method: 'HEAD',
        url: '/',
      } as NextApiRequest,
      response as any as NextApiResponse,
    );
    expect(response.statusCode).toBe(200);
  });
  test('post', async () => {
    creator.post((req, res) => {
      res.status(200).send(testValueResponse);
    });
    await creator.end()(
      {
        method: 'POST',
        url: '/',
      } as NextApiRequest,
      response as any as NextApiResponse,
    );
    expect(response.statusCode).toBe(200);
    expect(response.data).toBe(testValueResponse);
  });
  test('put', async () => {
    creator.put((req, res) => {
      res.status(200).send(testValueResponse);
    });
    await creator.end()(
      {
        method: 'PUT',
        url: '/',
      } as NextApiRequest,
      response as any as NextApiResponse,
    );
    expect(response.statusCode).toBe(200);
    expect(response.data).toBe(testValueResponse);
  });
  test('delete', async () => {
    creator.delete((req, res) => {
      res.status(200).send(testValueResponse);
    });
    await creator.end()(
      {
        method: 'DELETE',
        url: '/',
      } as NextApiRequest,
      response as any as NextApiResponse,
    );
    expect(response.statusCode).toBe(200);
    expect(response.data).toBe(testValueResponse);
  });
  test('patch', async () => {
    creator.patch((req, res) => {
      res.status(200).send(testValueResponse);
    });
    await creator.end()(
      {
        method: 'PATCH',
        url: '/',
      } as NextApiRequest,
      response as any as NextApiResponse,
    );
    expect(response.statusCode).toBe(200);
    expect(response.data).toBe(testValueResponse);
  });
});
test('unknown method', async () => {
  await creator.end()(
    {
      method: 'GET',
      url: '/',
    } as NextApiRequest,
    response as any as NextApiResponse,
  );
  expect(response.statusCode).toBe(405);
});

test('mixin catch', async () => {
  creator
    .mixin((req, res) => {
      res.status(401).send('oops');
    })
    .getMock(testValueResponse);
  await creator.end()(
    {
      method: 'GET',
      url: '/',
    } as NextApiRequest,
    response as any as NextApiResponse,
  );
  expect(response.statusCode).toBe(401);
  expect(response.data).toBe('oops');
});
test('mixin miss', async () => {
  creator.mixin((req, res) => {}).getMock(testValueResponse);
  await creator.end()(
    {
      method: 'GET',
      url: '/',
    } as NextApiRequest,
    response as any as NextApiResponse,
  );
  expect(response.statusCode).toBe(200);
  expect(response.data).toBe(testValueResponse);
});

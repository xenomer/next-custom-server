import { EventEmitter } from 'events';
jest.mock('node-ipc', () => {
  return {
    IPC: class {
      public config = {
        socketRoot: '/tmp/',
        appspace: '',
        id: '',
      };
      public server = new (class extends EventEmitter {
        start() {}
        public override emit(serverProp: any, name: string, value?: any): boolean {
          return super.emit(name, value !== undefined ? JSON.parse(JSON.stringify(value)) : undefined);
        }
      })();
      public connectTo(id: string, path: string) {}
      public disconnect(id: string) {}
      public serve(path: string, fn: Function) {
        fn();
      }
    },
  };
});
import { nanoid } from 'nanoid';
import { serializeError } from 'serialize-error';
import { RemoteFunctionServer, RemoteFunctionThisContext } from '../RemoteFunctionServer';
import { Readable } from 'stream';

let serverId: string;
let fnId: string;
let testFn: jest.Mock;
let clientFnListener: jest.Mock;
let server: RemoteFunctionServer;

beforeEach(() => {
  serverId = nanoid(6);
  fnId = nanoid(6);
  testFn = jest.fn();
  clientFnListener = jest.fn();
  server = new RemoteFunctionServer(serverId);
  server['ipc'].server.on(fnId, clientFnListener);
  server.register('test', testFn);
  server.start();
});
const testFnCall = async (name: string, ...args: any[]) => {
  await server['handleFunctionCall'](
    {
      callId: fnId,
      name,
      args,
    },
    {}, // socket stub
  );
};

describe('remote functions', () => {
  test('return value', async () => {
    testFn.mockReturnValue(true);
    const arg = [1, 'argument'];
    await testFnCall('test', ...arg);

    expect(testFn).toHaveBeenCalledWith(...arg);
    expect(clientFnListener).toHaveBeenCalledWith({ error: null, value: true });
  });

  test('throw error', async () => {
    const testError = new Error('test error');
    testFn.mockImplementation(() => {
      throw testError;
    });
    await testFnCall('test');

    expect(testFn).toHaveBeenCalledWith();
    expect(clientFnListener).toHaveBeenCalledWith({ error: serializeError(testError), value: null });
  });

  test('missing function', async () => {
    await testFnCall('notFound');

    expect(clientFnListener).toHaveBeenCalled();
    expect(clientFnListener).toHaveBeenCalledWith({
      error: expect.objectContaining({
        message: 'remote function notFound not found',
      }),
      value: null,
    });
  });
});

describe('remote functions features', () => {
  test('send()', async () => {
    const sendValues = ['test value!', 15];
    testFn.mockImplementation(function (this: RemoteFunctionThisContext) {
      sendValues.forEach((value) => this.send(value));
    });
    const sendListener = jest.fn();
    server['ipc'].server.on(`__${fnId}__send`, sendListener);
    await testFnCall('test');

    expect(testFn).toHaveBeenCalledWith();
    expect(clientFnListener).toHaveBeenCalled();
    sendValues.forEach((value, i) => expect(sendListener).toHaveBeenNthCalledWith(i + 1, value));
  });

  test('pipe()', async () => {
    const sendValues = ['test value!', 'test value 2'];
    testFn.mockImplementation(function (this: RemoteFunctionThisContext) {
      const srcStream = new Readable();
      this.pipe(srcStream);
      sendValues.forEach((value) => srcStream.push(value));
      srcStream.push(null);

      // the client would normally handle
      // disposing of the stream, but we
      // need to do that here instead
      srcStream.destroy();
    });
    const sendListener = jest.fn();
    server['ipc'].server.on(`__${fnId}__send`, sendListener);
    await testFnCall('test');

    const stream = new Readable();
    const dataArray: any[] = [];
    let onOpen: jest.Mock = null as any;
    let onData: jest.Mock = null as any;
    let onClose: jest.Mock = null as any;
    const streamWaiter = new Promise((resolve) => {
      onOpen = jest.fn(() => {
        server['ipc'].server.off(`__${fnId}__stream_open`, onOpen);
      });
      onData = jest.fn((chunk: any) => {
        const buffer = Buffer.from(chunk);
        stream.push(buffer);
        dataArray.push(buffer.toString());
      });
      onClose = jest.fn(() => {
        stream.push(null);
        server['ipc'].server.off(`__${fnId}__stream_data`, onData);
        server['ipc'].server.off(`__${fnId}__stream_close`, onClose);
        resolve(undefined);
      });
      server['ipc'].server.on(`__${fnId}__stream_open`, onOpen);
      server['ipc'].server.on(`__${fnId}__stream_data`, onData);
      server['ipc'].server.on(`__${fnId}__stream_close`, onClose);
    });

    await testFnCall('test');
    await streamWaiter;

    expect(testFn).toHaveBeenCalledWith();
    expect(clientFnListener).toHaveBeenCalled();
    expect(onOpen).toHaveBeenCalled();
    expect(onData).toHaveBeenCalled();
    expect(onClose).toHaveBeenCalled();
    expect(stream).toBeInstanceOf(Readable);
    expect(dataArray).toMatchObject(sendValues);
  });
});

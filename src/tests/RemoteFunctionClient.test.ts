import { EventEmitter } from 'events';
jest.mock('node-ipc', () => {
  return {
    IPC: class {
      public config = {
        socketRoot: '/tmp/',
        appspace: '',
        id: '',
      };
      public of: Record<string, EventEmitter> = {};
      public connectTo(id: string, path: string) {
        this.of[id] = new (class extends EventEmitter {
          start() {}
          public override emit(server: any, name: string, ...args: any[]): boolean {
            if (typeof server === 'string') {
              if (server === '__fn') {
                const { callId, args: argsProp } = name as any;
                setTimeout(() => super.emit(callId, argsProp[0]), 0);
              }
            }
            return super.emit(name, args);
          }
        })();
      }
      public disconnect(id: string) {}
      public serve(path: string, fn: Function) {
        fn();
      }
    },
  };
});
import { nanoid } from 'nanoid';
import { serializeError } from 'serialize-error';
import { RemoteFunctionClient } from '../RemoteFunctionClient';

interface TestInterface {
  test: (returnValue: { error: any; value: any }) => Promise<boolean>;
}

test('simple function', async () => {
  const id = nanoid(6);
  const client = RemoteFunctionClient.createProxy<TestInterface>(id);
  const result = client.test({ error: null, value: 123 });

  expect(result).resolves.toBe(123);

  // client.close();
});
test('error function', async () => {
  const id = nanoid(6);
  const client = RemoteFunctionClient.createProxy<TestInterface>(id);
  const result = client.test({ error: serializeError(new Error()), value: null });

  await expect(result).rejects.toThrow(Error);

  // client.close();
});

import { RemoteFunctionServer } from '../RemoteFunctionServer';
import { RemoteFunctionClient } from '../RemoteFunctionClient';
import { promisify } from 'util';
import { nanoid } from 'nanoid';
const wait = promisify(setTimeout);

interface IServerFn {
  fn(val: any): Promise<any>;
}

describe('lifecycle', () => {
  let serverId: string;
  let server: RemoteFunctionServer;
  let client: RemoteFunctionClient<IServerFn>;
  test('connect', async () => {
    serverId = nanoid(6);
    server = new RemoteFunctionServer(serverId);
    server.register('fn', (val: any) => val);
    server.start();
    await wait(100);
    client = new RemoteFunctionClient(serverId);

    await client.connect();

    expect(client['ipc'].of[serverId]).toBeDefined();
  });

  test('doFunctionCall', async () => {
    const argument = 123;
    const result = await client.doFunctionCall('fn', [argument]);
    expect(result).toBe(argument);
  });

  test('proxy call', async () => {
    const argument = 321;
    const result = await client.proxy.fn(argument);
    expect(result).toBe(argument);
  });

  test('disconnect', async () => {
    let disconnected = false;
    server['ipc'].server.on('socket.disconnected', () => {
      disconnected = true;
    });

    client.close();
    await wait(100);

    expect(disconnected).toBe(true);
    expect(client['ipc'].of[serverId]).toBeUndefined();
    expect((server['ipc']['server'] as any)['sockets']).toHaveLength(0);
  });
});

test('createProxy', async () => {
  const serverId = nanoid(6);
  const server = new RemoteFunctionServer(serverId);
  server.register('fn', (val: any) => val);
  server.start();
  await wait(100);
  const proxy = RemoteFunctionClient.createProxy<IServerFn>(serverId);

  const argument = 132;
  const result = await proxy.fn(argument);
  await wait(100);

  expect(result).toBe(argument);
  expect((server['ipc']['server'] as any)['sockets']).toHaveLength(0);
});

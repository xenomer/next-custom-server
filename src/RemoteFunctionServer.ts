import ipc from 'node-ipc';
import createDebug, { Debugger } from 'debug';
import { serializeError } from 'serialize-error';
import { Readable } from 'stream';
import { BooleanSupportOption } from 'prettier';

export interface RemoteFunctionServerOptions {
  silent?: boolean;
  maxConnections?: number;
}
export interface SerializedFunctionCallDefinition {
  callId: string;
  name: string;
  args: any[];
}

export interface RemoteFunctionThisContext {
  send: (data: any) => void;
  pipe: (stream: Readable) => void;
}

export class RemoteFunctionServer {
  public id: string;
  public path: string;
  public throwOnServerSide = false;

  private debug: Debugger;
  private fnMap: Record<string, Function> = {};
  private ipc;

  constructor(id: string = 'default', options?: RemoteFunctionServerOptions) {
    this.debug = createDebug('next-custom-server-manager' + ':fnServer' + (id === 'default' ? '' : `:${id}`));
    this.id = id;
    this.ipc = new ipc.IPC();
    this.ipc.config.silent = options?.silent ?? true;
    this.ipc.config.id = id;
    this.ipc.config.maxConnections = options?.maxConnections ?? 500;
    this.path = this.ipc.config.socketRoot + this.ipc.config.appspace + this.ipc.config.id;
    this.ipc.serve(this.path, this.serve.bind(this));
    this.ipc.server.on('connect', () => {
      this.debug('client connected');
    });
    this.ipc.server.on('socket.disconnected', (socket) => {
      this.debug('client disconnected');
    });
  }

  public register(name: string, fn: (this: RemoteFunctionThisContext, ...args: any[]) => any) {
    this.fnMap[name] = fn;
  }
  public start() {
    this.debug('server started');
    this.ipc.server.start();
  }

  private serve() {
    this.ipc.server.on('__fn', this.handleFunctionCall.bind(this));
  }
  private async handleFunctionCall(config: SerializedFunctionCallDefinition, socket: any) {
    const signature = `%s(${config.args.map((a) => '%o').join(', ')})`;
    const signatureArgs = [config.name, ...config.args];
    this.debug(`running ${signature}`, ...signatureArgs);
    const fnHandler = this.fnMap[config.name];
    if (fnHandler) {
      let error = null;
      let value = null;
      try {
        const thisValue: RemoteFunctionThisContext = {
          send: (data) => {
            this.ipc.server.emit(socket, `__${config.callId}__send`, data);
          },
          pipe: (stream) => {
            this.ipc.server.emit(socket, `__${config.callId}__stream_open`);
            stream.on('data', (chunk: Buffer) => {
              this.ipc.server.emit(socket, `__${config.callId}__stream_data`, chunk);
            });
            stream.on('close', () => {
              this.ipc.server.emit(socket, `__${config.callId}__stream_close`);
            });
          },
        };
        value = await fnHandler.apply(thisValue, config.args);
      } catch (e) {
        if (this.throwOnServerSide) {
          throw e;
        } else {
          error = serializeError(e);
        }
      }
      this.debug(`${signature} => ${error ? 'error' : 'success'}`, ...signatureArgs);
      this.ipc.server.emit(socket, config.callId, {
        value,
        error,
      });
    } else {
      this.debug('function %s not found', config.name);
      this.ipc.server.emit(socket, config.callId, {
        error: serializeError(new Error('remote function ' + config.name + ' not found')),
        value: null,
      });
    }
  }
}

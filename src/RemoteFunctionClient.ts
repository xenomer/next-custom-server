import { SerializedFunctionCallDefinition } from './RemoteFunctionServer';
import { nanoid } from 'nanoid';
import ipc from 'node-ipc';
import { deserializeError } from 'serialize-error';
import createDebug, { Debugger } from 'debug';
import { Readable, PassThrough } from 'stream';
import { promisify } from 'util';
// import { QueueReadableStream } from './QueueReadableStream';

export interface RemoteFunctionClientOptions {
  silent?: boolean;
}
export interface RemoteFunctionClientProxy {
  close(): void;
}

export interface RemoteFunctionInvokeOptions {
  onSend?: (data: any) => void;
  onStream?: (stream: Readable) => void;
}

export class RemoteFunctionClient<T extends object> {
  public id: string;
  public path: string;
  public proxy: T;
  private overrides: Record<string, Function> = {};
  private ipc;
  private server;
  private debug: Debugger;
  private connectPromise: Promise<undefined>;
  constructor(id: string = 'default', options?: RemoteFunctionClientOptions) {
    this.debug = createDebug('next-custom-server-manager' + ':fnClient' + (id === 'default' ? '' : `:${id}`));
    this.id = id;
    this.ipc = new ipc.IPC();
    this.ipc.config.silent = options?.silent ?? true;
    this.ipc.config.id = id;
    this.path = this.ipc.config.socketRoot + this.ipc.config.appspace + this.ipc.config.id;
    this.proxy = new Proxy({} as object, {
      get: this.handleProxyGet.bind(this),
    });
    this.connectPromise = new Promise((resolve) => {
      this.ipc.connectTo(this.id, this.path, () => {
        resolve(undefined);
      });
    });
    this.server = this.ipc.of[id];
  }

  public async connect() {
    return this.connectPromise;
  }

  public static createProxy<T extends object>(id: string = 'default'): T {
    return new Proxy(
      {},
      {
        get(target, name) {
          if (typeof name === 'symbol') {
            return undefined;
          } else {
            return async (...args: any) => {
              const server = new RemoteFunctionClient<T>(id);
              const result = await server.doFunctionCall(name, args);
              server.close();
              return result;
            };
          }
        },
      },
    ) as T;
  }

  public close() {
    this.ipc.disconnect(this.id);
  }

  public override(name: string, fn: Function) {
    this.overrides[name] = fn;
  }

  private once(name: string, fn: Function) {
    const handler = (...arg: any[]) => {
      this.server.off(name, handler);
      fn(...arg);
    };
    this.server.on(name, handler);
  }
  private handleProxyGet(target: any, name: string) {
    // prevent async from thinking this is promise-like
    if (name === 'then') {
      return;
    }
    // check overrides
    if (this.overrides[name]) {
      return this.overrides[name];
    }

    return (...args: any[]) => this.doFunctionCall(name, args);
  }

  private handleStreamReceive(fnId: string, onStream: RemoteFunctionInvokeOptions['onStream']) {
    const stream = new PassThrough();
    const onOpen = () => {
      onStream?.(stream);
      this.server.off(`__${fnId}__stream_open`, onOpen);
    };
    const onData = (chunk: any) => {
      const buffer = Buffer.from(chunk);
      stream.push(buffer);
    };
    const onClose = () => {
      stream.push(null);
      this.server.off(`__${fnId}__stream_data`, onData);
      this.server.off(`__${fnId}__stream_close`, onClose);
    };
    this.server.on(`__${fnId}__stream_open`, onOpen);
    this.server.on(`__${fnId}__stream_data`, onData);
    this.server.on(`__${fnId}__stream_close`, onClose);
    return stream;
  }

  public doFunctionCall(
    name: string,
    args: any[],
    { onSend, onStream }: RemoteFunctionInvokeOptions = {},
  ): Promise<any> {
    this.debug(`${name}(${args.map((a) => '%o').join(', ')})?`, ...args);
    const fnId = nanoid(6);
    return new Promise((resolve, reject) => {
      const onSendHandler = (data: any) => {
        onSend?.(data);
      };
      let stream: Readable | null = null;
      if (onSend) {
        this.server.on(`__${fnId}__send`, onSendHandler);
      }
      if (onStream) {
        stream = this.handleStreamReceive(fnId, onStream);
      }
      this.server.emit('__fn', {
        callId: fnId,
        name,
        args,
      } as SerializedFunctionCallDefinition);
      this.once(fnId, async ({ error, value }: { error: any; value: any }) => {
        const returnString = error ? `! %o` : `-> %o`;
        this.debug(
          `${name}(${args.map((a) => '%o').join(', ')}) ${returnString}`,
          ...args,
          (error?.message ?? error) || value,
        );
        if (onSend) {
          this.server.off(`__${fnId}__send`, onSendHandler);
        }

        if (stream) {
          stream.destroy();
        }

        if (error) {
          reject(deserializeError(error));
        } else {
          resolve(value);
        }
      });
    });
  }
}

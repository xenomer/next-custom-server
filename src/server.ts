import { createServer, IncomingMessage, ServerResponse, Server } from 'http';
import { parse } from 'url';
import next from 'next';
import { spawn, spawnSync, ChildProcess } from 'child_process';
import createDebug, { Debugger } from 'debug';
import { NextServer } from 'next/dist/server/next';
import _ from 'lodash';

const defaultDev = process.env.NODE_ENV !== 'production';
const defaultPort = Number.parseInt(process.env.PORT || '3000') || 3000;

let globalDebugIndex = 0;

export interface NextCustomServerConfig {
  /**
   * Port of the Next server
   */
  port?: number;

  /**
   * Whether dev or production
   */
  dev?: boolean;

  /**
   * List of tasks to execute when the server starts
   */
  tasks: NextCustomServerTaskConfig[];

  /**
   * Handle requests manually. Return true to prevent
   * Next.JS from handling the request.
   */
  handleRequest?: (
    req: import('http').IncomingMessage,
    res: import('http').ServerResponse,
    parsedUrl: import('url').UrlWithParsedQuery,
  ) => Promise<boolean | undefined>;
}

export type NextCustomServerTaskConfig = {
  /**
   * Name of the task
   */
  name: string;
  /**
   * In where environments to run this task
   */
  env?: ('production' | 'development')[];

  /**
   * Function to run when the task executes
   *
   * Either `run` or `command` has to be defined in the config
   */
  run?: () => any | PromiseLike<any>;

  /**
   * Full string command to execute, e.g. "npm run build"
   *
   * Either `run` or `command` has to be defined in the config
   */
  command?: string;
  /**
   * STDIO of the started process
   * This has no effect with `run` method
   */
  stdio?: import('child_process').StdioOptions;
  /**
   * Whether to wait for exit before continuing the pipeline
   * This has no effect with `run` method
   */
  executeSync?: boolean;
};

export interface NextCustomServerTask {
  config: NextCustomServerTaskConfig;
  process?: import('child_process').ChildProcess;
}

/**
 * A Next.JS server with custom tasks pipeline system.
 */

export class NextCustomServer {
  public config: NextCustomServerConfig;
  public app?: NextServer;
  public server?: Server;
  public tasks?: NextCustomServerTask[];

  private handleAppRequest?: ReturnType<NextServer['getRequestHandler']>;
  private debug: Debugger;

  constructor(config: NextCustomServerConfig) {
    this.debug = createDebug('next-custom-server-manager:server' + (globalDebugIndex ? `:${globalDebugIndex}` : ''));
    globalDebugIndex++;
    this.config = _.defaults(config, {
      dev: defaultDev,
      port: defaultPort,
    });
  }

  async injectProcess(process: ChildProcess, name: string) {
    process.on('exit', (code, signal) => {
      this.debug('task %s has exited with code %o (%s)', name, code, signal);
      if (code === 0) {
        console.log('task %s has exited with code %d', name, code);
      } else {
        console.error('task %s has exited with code %o', name, code ?? signal);
      }
    });
    process.on('error', (err) => {
      this.debug('task %s has exited with error %O', name, err);
      console.error('task %s has exited with error %o', name, err);
    });
  }

  async handleRequest(req: IncomingMessage, res: ServerResponse) {
    const parsedUrl = parse(String(req.url), true);
    const didHandle = this.config.handleRequest?.(req, res, parsedUrl);
    if (!didHandle) {
      this.handleAppRequest?.(req, res, parsedUrl);
    }
  }

  async start() {
    this.debug('Starting next-custom-server-manager');

    const env = this.config.dev ? 'development' : 'production';
    this.debug('env: %o', env);

    const tasks: NextCustomServerTask[] = [];
    this.debug(
      'running %d/%d tasks',
      this.config.tasks.filter((t) => t.env?.includes(env) ?? true).length,
      this.config.tasks.length,
    );
    for (const _taskConfig of this.config.tasks) {
      const taskConfig: NextCustomServerTaskConfig = _.defaults<
        NextCustomServerTaskConfig,
        Partial<NextCustomServerTaskConfig>
      >(_taskConfig, {
        stdio: 'inherit',
        executeSync: false,
        env: ['development', 'production'],
      });

      if (taskConfig.env?.includes(env)) {
        this.debug('task %o: %o', taskConfig.name, _.omit(taskConfig, ['run']));
        if (taskConfig.run) {
          await taskConfig.run();
          tasks.push({
            config: taskConfig,
          });
        } else if (taskConfig.command) {
          const commandSplit = taskConfig.command.split(' ');
          const command = _.head(commandSplit) || '';
          const args = _.tail(commandSplit);

          if (taskConfig.executeSync) {
            spawnSync(command, args, {
              stdio: taskConfig.stdio,
            });
          } else {
            const process = spawn(command, args, {
              stdio: taskConfig.stdio,
            });
            this.injectProcess(process, taskConfig.name);
            tasks.push({
              config: taskConfig,
              process,
            });
          }
        } else {
          throw new Error(`Either 'run' or 'command' has to be defined in the task config (${taskConfig.name})`);
        }
      }
    }
    this.tasks = tasks;

    this.app = next(this.config);
    this.handleAppRequest = this.app.getRequestHandler();
    await this.app.prepare();

    this.server = createServer(this.handleRequest.bind(this));
    this.server.listen(this.config.port, () => {
      this.debug(`listening on http://localhost:%d`, this.config.port);
    });
    this.server.on('close', () => {
      this.debug('closed');
    });
  }

  public async stop() {
    this.server?.close();
    await this.app?.close();
  }
}
